# What is Onvif IP Camera Viewer (C#) #

This is an Onvif IP Camera Manager that is a fully-functional camera software written in C#.NET that can be used to control your whole IP surveillance / monitoring system by providing all the most useful USB webcam and IP camera features. Due to its great functionalities, this Onvif IP Camera viewer (device manager) allows you to:

* Connect to more IP cameras, ONVIF IP cameras and USB webcams and display their image
* Manage the PTZ (Pan-Tilt-Zoom) functionality (move up, down, right, left and even zoom)
* Adjust the camera image (saturation, contrast, brightness, backlight, white balance)
* Take video recordings and snapshots
* Improve your alarm system with motion detection (you can send snapshots/videos as alerts in email; you can upload the snapshots/videos to FTP server; you can implement alarm notifications through a VoIP video phone call)
* Send and receive IP camera stream to/ from a SIP video phone call
* Perform general settings (time, name, location etc.) even remotely
* Implement an Onvif IP camera server and provide Onvif service
* Carry out security configurations to protect your IP camera against the malicious non-authorized persons (including setting the visibility of the IP camera on the network; network settings like IP, netmask, gateway, DNS, NTP etc. in DHCP or manually; and user management to authenticate)
* Stream IP camera image to a website and mobile devices (Android, iOS or Windows Phone smartphones and tablets)
* Find / discover ONVIF IP cameras and USB webcameras on your LAN
* Log in order to follow the events of the IP camera

After downloading this demo program, feel free to modify its source code according to your needs. For this purpose you need to download Ozeki Camera SDK that can be used to build webcam and IP camera solutions efficiently in C#.NET. After you have installed this software development kit, you can use it immediately for implementing C# camera solutions in order to improve your security monitoring system.

# For USB webcam and IP camera developments (C#) #
* [Onvif Camera Software Development Kit](http://www.camera-sdk.com/)
* [How to implement an Onvif IP Camera Manager in C#](http://www.camera-sdk.com/p_27-onvif-ip-camera-manager-exe-demo-onvif.html)
* [C# Camera Video Tutorials on how to build a USB webcam and IP camera viewer](http://www.camera-sdk.com/p_240-c-camera-video-tutorials-onvif.html)